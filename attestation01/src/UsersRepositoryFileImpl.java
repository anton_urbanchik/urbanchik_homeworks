import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFileImpl implements UsersRepository {

    private String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Создает список Users из файла
     *
     * @return ArrayList Список Users
     */
    private List<User> createUserList() {
        List<User> userList = new ArrayList<>();
        try (Reader reader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(reader)) {
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                int id = Integer.parseInt(parts[0]);
                String name = parts[1];
                int userAge = Integer.parseInt(parts[2]);
                boolean isWorker = Boolean.parseBoolean(parts[3]);
                User newUser = new User(id, name, userAge, isWorker);
                userList.add(newUser);
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            System.err.println("Ошибка чтения " + e.getMessage());
        }
        return userList;
    }

    /**
     * Перезаписывает файл users.txt после изменения
     *
     * @param userList Список Users
     */
    private void overwriteFile(List<User> userList) {
        try (Writer writer = new FileWriter("users.txt", false);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            userList.forEach(writeUser -> {
                try {
                    bufferedWriter.write(writeUser.getId() + "|" + writeUser.getName() + "|" + writeUser.getAge() + "|" + writeUser.isWorker() + "\n");
                } catch (IOException e) {
                    System.err.println("Ошибка записи из списка " + e.getMessage());
                }
            });
        } catch (IOException e) {
            System.err.println("Ошибка записи " + e.getMessage());
        }
    }

    @Override
    public User findById(int targetId) {
        List<User> userList = createUserList();
        User targetUser = userList
                .stream()
                .filter(user -> user.getId() == targetId)
                .findFirst()
                .orElse(null);

        if (targetUser == null) {
            System.err.println("Пользователя с таким id не существует");
            return null;
        }
        return targetUser;
    }

    @Override
    public void update(User user) {
        List<User> userList = createUserList();
        for (User changeUser : userList) {
            if (changeUser.getId() == user.getId()) {
                userList.set(userList.indexOf(changeUser), user);
            }
        }
        overwriteFile(userList);
        System.out.println("Файл users.txt обновлен");
    }
}
