
public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");

        User user = usersRepository.findById(2);

        user.setName("Виктор");
        user.setAge(50);

        usersRepository.update(user);
    }
}
