package urbanchik.attestation.pizzashop.forms;

import lombok.Data;

@Data
public class PizzaForm {
    private String name;
    private Integer cost;
    private String size;
}
