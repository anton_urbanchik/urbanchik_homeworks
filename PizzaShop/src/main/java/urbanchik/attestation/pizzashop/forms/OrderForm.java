package urbanchik.attestation.pizzashop.forms;

import lombok.Data;
import urbanchik.attestation.pizzashop.models.User;

@Data
public class OrderForm {
    private Integer id;
    private User user;
    private Integer resultCost;
}
