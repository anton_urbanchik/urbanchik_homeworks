package urbanchik.attestation.pizzashop.forms;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class UsersForm {
    @NotNull
    @Length(max = 20)
    private String city;

    @NotNull
    @Length(max = 20)
    private String address;
}
