package urbanchik.attestation.pizzashop.forms;

import lombok.Data;
import urbanchik.attestation.pizzashop.models.Cart;
import urbanchik.attestation.pizzashop.models.Pizza;

@Data
public class CartItemForm {
    private Cart cart;
    private Pizza pizza;
    private String name;
    private Integer cost;
    private String size;
}
