package urbanchik.attestation.pizzashop.forms;

import lombok.Data;
import urbanchik.attestation.pizzashop.models.CartItem;
import urbanchik.attestation.pizzashop.models.User;

import java.util.List;

@Data
public class CartForm {
    private Integer id;
    private List<CartItem> cartItemList;
    private User user;
    private Integer sumCost;
}
