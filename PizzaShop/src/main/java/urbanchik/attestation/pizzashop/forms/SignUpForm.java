package urbanchik.attestation.pizzashop.forms;

import lombok.Data;

@Data
public class SignUpForm {
    private String firstName;
    private String city;
    private String address;
    private String email;
    private String password;
}

