package urbanchik.attestation.pizzashop.services;

import urbanchik.attestation.pizzashop.forms.OrderForm;
import urbanchik.attestation.pizzashop.models.Order;
import urbanchik.attestation.pizzashop.models.User;

public interface OrderService {

    /**
     * создает заказ
     *
     * @param orderForm форма заказа
     * @param user      текущий пользователь
     */
    void createUserOrder(OrderForm orderForm, User user);

    /**
     * возвращет заказ, авторизованного в данный момент пользователя
     *
     * @return заказ
     */
    Order getCurrentOrder();

    /**
     * устанавливает сумму заказа
     */
    void setResultCost();

    /**
     * устанавливает дату и время заказа
     */
    void setDateTime();
}
