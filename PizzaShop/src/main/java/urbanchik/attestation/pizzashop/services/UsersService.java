package urbanchik.attestation.pizzashop.services;

import urbanchik.attestation.pizzashop.forms.UsersForm;
import urbanchik.attestation.pizzashop.models.User;

import java.util.List;

public interface UsersService {
    /**
     * возвращает авторизованного в данных момент пользователя
     *
     * @return текущего пользователя
     */
    User getCurrentUser();

    /**
     * возвращает список всех пользователей
     *
     * @return список пользователей
     */
    List<User> getAllUsers();

    /**
     * обновляет и сохраняет изменения о пользователе в базе данных
     *
     * @param userId    id изменяемого пользователя
     * @param usersForm форма пользователя
     */
    void update(Integer userId, UsersForm usersForm);
}
