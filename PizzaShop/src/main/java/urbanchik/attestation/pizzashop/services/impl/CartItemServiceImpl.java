package urbanchik.attestation.pizzashop.services.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import urbanchik.attestation.pizzashop.forms.CartItemForm;
import urbanchik.attestation.pizzashop.models.Cart;
import urbanchik.attestation.pizzashop.models.CartItem;
import urbanchik.attestation.pizzashop.models.Pizza;
import urbanchik.attestation.pizzashop.models.User;
import urbanchik.attestation.pizzashop.repositories.CartItemRepository;
import urbanchik.attestation.pizzashop.repositories.CartRepository;
import urbanchik.attestation.pizzashop.repositories.PizzaRepository;
import urbanchik.attestation.pizzashop.repositories.UsersRepository;
import urbanchik.attestation.pizzashop.services.CartItemService;

import java.util.Optional;


@RequiredArgsConstructor
@Component
public class CartItemServiceImpl implements CartItemService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CartItemServiceImpl.class.getName());

    private final CartItemRepository cartItemRepository;
    private final PizzaRepository pizzaRepository;
    private final CartRepository cartRepository;
    private final UsersRepository usersRepository;

    @Override
    public void createCartItem(CartItemForm form, Integer pizzaId, Integer cartId) {
        LOGGER.info("Берем пиццу из репозитория");

        Pizza pizza = pizzaRepository.getById(pizzaId);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();

        Optional<User> user = usersRepository.findByEmail(currentPrincipalName);

        Cart cart = cartRepository.getCartByUser(user);

        CartItem cartItem = CartItem.builder()
                .cart(cart)
                .pizza(pizza)
                .name(pizza.getName())
                .cost(pizza.getCost())
                .size(pizza.getSize())
                .build();

        cartItemRepository.save(cartItem);
        LOGGER.info("Добавляем созданный элемент корзины в корзину текущего пользователя");
        cart.addToCart(cartItem);
    }
}
