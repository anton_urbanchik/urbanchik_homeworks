package urbanchik.attestation.pizzashop.services;

import urbanchik.attestation.pizzashop.forms.SignUpForm;
import urbanchik.attestation.pizzashop.models.User;

public interface SignUpService {
    /**
     * создает пользователя при регистрации
     *
     * @param form форма регистрации нового пользователя
     * @return пользователя
     */
    User signUpUser(SignUpForm form);
}
