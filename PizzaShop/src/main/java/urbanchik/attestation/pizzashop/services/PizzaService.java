package urbanchik.attestation.pizzashop.services;

import urbanchik.attestation.pizzashop.forms.PizzaForm;
import urbanchik.attestation.pizzashop.models.Pizza;

import java.util.List;

public interface PizzaService {

    /**
     * возвращает список всех пицц
     *
     * @return список существующих пицц
     */
    List<Pizza> getAllPizza();

    /**
     * создает сущность "пицца"
     *
     * @param form форма пиццы
     */
    void addPizza(PizzaForm form);
}
