package urbanchik.attestation.pizzashop.services.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import urbanchik.attestation.pizzashop.forms.PizzaForm;
import urbanchik.attestation.pizzashop.models.Pizza;
import urbanchik.attestation.pizzashop.repositories.PizzaRepository;
import urbanchik.attestation.pizzashop.services.PizzaService;

import java.util.List;

@RequiredArgsConstructor
@Component
public class PizzaServiceImpl implements PizzaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PizzaServiceImpl.class.getName());

    private final PizzaRepository pizzaRepository;

    @Override
    public List<Pizza> getAllPizza() {
        LOGGER.info("Возвращаем список всех пицц");
        return pizzaRepository.findAll();
    }

    @Override
    public void addPizza(PizzaForm form) {
        Pizza pizza = Pizza.builder()
                .name(form.getName())
                .cost(form.getCost())
                .size(form.getSize())
                .build();
        LOGGER.info("Сохраняем созданную пиццу " + pizza.getName());
        pizzaRepository.save(pizza);
    }
}
