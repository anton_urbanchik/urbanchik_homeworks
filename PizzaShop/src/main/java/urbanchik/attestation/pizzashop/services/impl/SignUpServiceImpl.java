package urbanchik.attestation.pizzashop.services.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import urbanchik.attestation.pizzashop.forms.SignUpForm;
import urbanchik.attestation.pizzashop.models.User;
import urbanchik.attestation.pizzashop.repositories.UsersRepository;
import urbanchik.attestation.pizzashop.services.SignUpService;

@RequiredArgsConstructor
@Component
public class SignUpServiceImpl implements SignUpService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SignUpServiceImpl.class.getName());

    private final PasswordEncoder passwordEncoder;
    private final UsersRepository usersRepository;

    @Override
    public User signUpUser(SignUpForm form) {
        LOGGER.info("Создаем пользователя");
        User user = User.builder()
                .firstName(form.getFirstName())
                .city(form.getCity())
                .address(form.getAddress())
                .email(form.getEmail())
                .role(User.Role.USER)
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .build();
        LOGGER.info("Добавляем пользователя " + user.getEmail() + " в репозиторий");
        usersRepository.save(user);
        return user;
    }
}
