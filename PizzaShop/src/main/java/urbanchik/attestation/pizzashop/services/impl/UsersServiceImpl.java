package urbanchik.attestation.pizzashop.services.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import urbanchik.attestation.pizzashop.forms.UsersForm;
import urbanchik.attestation.pizzashop.models.User;
import urbanchik.attestation.pizzashop.repositories.UsersRepository;
import urbanchik.attestation.pizzashop.services.UsersService;

import java.util.List;

@RequiredArgsConstructor
@Component
public class UsersServiceImpl implements UsersService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsersServiceImpl.class.getName());

    private final UsersRepository usersRepository;

    @Override
    public User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        LOGGER.warn("Возвращаем текущего пользователя");
        return usersRepository.findByEmail(currentPrincipalName).orElseThrow(() -> new NullPointerException(currentPrincipalName));
    }

    @Override
    public List<User> getAllUsers() {
        LOGGER.info("Возвращаем список всех пользователей");
        return usersRepository.findAll();
    }

    @Override
    public void update(Integer userId, UsersForm usersForm) {
        User user = usersRepository.getById(userId);
        user.setCity(usersForm.getCity());
        user.setAddress(usersForm.getAddress());
        LOGGER.info("Обновляем иформацию о пользователе " + user.getEmail());
        usersRepository.save(user);
    }
}
