package urbanchik.attestation.pizzashop.services;


import urbanchik.attestation.pizzashop.forms.CartItemForm;

public interface CartItemService {
    /**
     * создает сущность "элемент корзины" в базе данных
     *
     * @param form    форма элемента корзины
     * @param pizzaId id пиццы
     * @param cartId  id корзины
     */
    void createCartItem(CartItemForm form, Integer pizzaId, Integer cartId);
}
