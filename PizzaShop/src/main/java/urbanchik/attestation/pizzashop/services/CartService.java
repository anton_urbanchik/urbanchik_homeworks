package urbanchik.attestation.pizzashop.services;

import urbanchik.attestation.pizzashop.forms.CartForm;
import urbanchik.attestation.pizzashop.models.Cart;
import urbanchik.attestation.pizzashop.models.CartItem;
import urbanchik.attestation.pizzashop.models.User;

import java.util.List;

public interface CartService {
    /**
     * обновляет сумму цен в корзине
     */
    void updateSumCost();

    /**
     * создает корзину покупок
     *
     * @param cartForm форма корзины
     * @param user     текущий пользователь
     */
    void createUserCart(CartForm cartForm, User user);

    /**
     * возвращает список элементов, содержащихся в корзине покупок
     *
     * @return список элементов корзины
     */
    List<CartItem> getAllCartItem();

    /**
     * возвращает сумму цен в корзине
     *
     * @return итоговая сумма
     */
    Integer getSumCost();

    /**
     * удаляет элемент в корзине
     *
     * @param cartItemId id элемента корзины
     */
    void deleteCartItem(Integer cartItemId);

    /**
     * очищает корзину
     *
     * @param cart корзина, которую очищаем
     */
    void clearCart(Cart cart);

    /**
     * возвращет корзину покупок авторизованного в данный момент пользователя
     *
     * @return текущая корзина
     */
    Cart getCurrentCart();
}
