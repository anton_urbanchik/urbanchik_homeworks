package urbanchik.attestation.pizzashop.services.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import urbanchik.attestation.pizzashop.forms.OrderForm;
import urbanchik.attestation.pizzashop.models.Cart;
import urbanchik.attestation.pizzashop.models.Order;
import urbanchik.attestation.pizzashop.models.User;
import urbanchik.attestation.pizzashop.repositories.OrderRepository;
import urbanchik.attestation.pizzashop.repositories.UsersRepository;
import urbanchik.attestation.pizzashop.services.CartService;
import urbanchik.attestation.pizzashop.services.OrderService;

import java.time.LocalDateTime;
import java.util.Optional;

@RequiredArgsConstructor
@Component
public class OrderServiceImpl implements OrderService {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImpl.class.getName());


    private final OrderRepository orderRepository;
    private final UsersRepository usersRepository;
    private final CartService cartService;


    @Override
    public void createUserOrder(OrderForm orderForm, User user) {
        Order order = Order.builder()
                .user(user)
                .build();
        LOGGER.info("Создаем заказ");
        orderRepository.save(order);
    }


    @Override
    public Order getCurrentOrder() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();

        Optional<User> user = usersRepository.findByEmail(currentPrincipalName);
        LOGGER.info("Возвращаем заказ текущего пользователя");
        return orderRepository.getOrderByUser(user);
    }

    @Override
    public void setResultCost() {
        Order order = getCurrentOrder();
        Cart cart = cartService.getCurrentCart();
        order.setResultCost(cart.getSumCost());
        LOGGER.info("Устанавливаем сумму заказа");
        orderRepository.save(order);
    }

    @Override
    public void setDateTime() {
        Order order = getCurrentOrder();
        order.setDateTime(LocalDateTime.now());
        LOGGER.info("Устанавливаем дату и время заказа");
        orderRepository.save(order);
    }
}
