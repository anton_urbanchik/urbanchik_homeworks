package urbanchik.attestation.pizzashop.services.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import urbanchik.attestation.pizzashop.forms.CartForm;
import urbanchik.attestation.pizzashop.models.Cart;
import urbanchik.attestation.pizzashop.models.CartItem;
import urbanchik.attestation.pizzashop.models.User;
import urbanchik.attestation.pizzashop.repositories.CartItemRepository;
import urbanchik.attestation.pizzashop.repositories.CartRepository;
import urbanchik.attestation.pizzashop.repositories.UsersRepository;
import urbanchik.attestation.pizzashop.services.CartService;

import java.util.List;
import java.util.Optional;


@RequiredArgsConstructor
@Component
public class CartServiceImpl implements CartService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CartServiceImpl.class.getName());


    private final CartRepository cartRepository;
    private final UsersRepository usersRepository;
    private final CartItemRepository cartItemRepository;

    private Integer countingSumCost(List<CartItem> cartItemList) {
        LOGGER.info("Считаем сумму цен");
        if (cartItemList.isEmpty()) {
            return 0;
        } else {
            Integer sumCost = 0;
            for (CartItem elem : cartItemList) {
                sumCost += elem.getCost();
            }
            return sumCost;
        }
    }

    @Override
    public Cart getCurrentCart() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();

        Optional<User> user = usersRepository.findByEmail(currentPrincipalName);
        LOGGER.info("Возвращаем корзину текущего пользователя");
        return cartRepository.getCartByUser(user);
    }

    @Override
    public void updateSumCost() {
        Cart cart = getCurrentCart();
        cart.setSumCost(countingSumCost(getAllCartItem()));
        LOGGER.info("Обновляем сумму цен");
        cartRepository.save(cart);
    }

    @Override
    public void createUserCart(CartForm cartForm, User user) {
        Cart cart = Cart.builder()
                .user(user)
                .build();
        LOGGER.info("Создание корзины");
        cartRepository.save(cart);
    }

    @Override
    public List<CartItem> getAllCartItem() {
        Cart cart = getCurrentCart();
        LOGGER.info("Возвращяем список элементов корзины, содержащихся в текущей корзине");
        return cart.getCartItemList();
    }

    @Override
    public Integer getSumCost() {
        LOGGER.info("Возвращаем сумму цен");
        return countingSumCost(getAllCartItem());
    }

    @Override
    public void deleteCartItem(Integer cartItemId) {
        LOGGER.info("Удаляем элемент корзины");
        cartItemRepository.deleteById(cartItemId);
    }

    @Override
    public void clearCart(Cart cart) {
        LOGGER.info("Полностью очищаем корзину");
        cartItemRepository.deleteByCart_Id(cart.getId());
    }

}
