package urbanchik.attestation.pizzashop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import urbanchik.attestation.pizzashop.models.User;

import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Integer> {
    Optional<User> findByEmail(String email);
}
