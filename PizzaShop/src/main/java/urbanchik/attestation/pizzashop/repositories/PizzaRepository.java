package urbanchik.attestation.pizzashop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import urbanchik.attestation.pizzashop.models.Pizza;

import java.util.Optional;

public interface PizzaRepository extends JpaRepository<Pizza, Integer> {
    Optional<Pizza> findById(Integer pizzaId);
}
