package urbanchik.attestation.pizzashop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import urbanchik.attestation.pizzashop.models.CartItem;

public interface CartItemRepository extends JpaRepository<CartItem, Integer> {
    @Transactional
    void deleteByCart_Id(Integer cartId);
}
