package urbanchik.attestation.pizzashop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import urbanchik.attestation.pizzashop.models.Cart;
import urbanchik.attestation.pizzashop.models.User;

import java.util.Optional;

public interface CartRepository extends JpaRepository<Cart, Integer> {
    Cart getCartByUser(Optional<User> user);
}
