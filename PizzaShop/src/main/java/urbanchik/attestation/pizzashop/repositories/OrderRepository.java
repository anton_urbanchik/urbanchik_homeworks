package urbanchik.attestation.pizzashop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import urbanchik.attestation.pizzashop.models.Order;
import urbanchik.attestation.pizzashop.models.User;

import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Integer> {
    Order getOrderByUser(Optional<User> user);
}
