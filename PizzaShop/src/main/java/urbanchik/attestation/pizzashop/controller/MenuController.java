package urbanchik.attestation.pizzashop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import urbanchik.attestation.pizzashop.forms.CartItemForm;
import urbanchik.attestation.pizzashop.models.Pizza;
import urbanchik.attestation.pizzashop.services.CartItemService;
import urbanchik.attestation.pizzashop.services.PizzaService;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class MenuController {

    private final PizzaService pizzaService;
    private final CartItemService cartItemService;

    @GetMapping("/menu")
    public String getMenuPage(Model model) {
        List<Pizza> pizzaList = pizzaService.getAllPizza();
        model.addAttribute("pizzaList", pizzaList);
        return "menu";
    }

    @PostMapping("/menu/{pizza-id}/cartItem")
    public String createCartItem(@PathVariable("pizza-id") Integer pizzaId, CartItemForm form, Integer cartId) {
        cartItemService.createCartItem(form, pizzaId, cartId);
        return "redirect:/menu";
    }
}
