package urbanchik.attestation.pizzashop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import urbanchik.attestation.pizzashop.forms.UsersForm;
import urbanchik.attestation.pizzashop.models.User;
import urbanchik.attestation.pizzashop.services.UsersService;

@Controller
@RequiredArgsConstructor
public class PersonalAccountController {

    private final UsersService usersService;

    @GetMapping("/personalAccount")
    public String getPersonalAccountPage(Model model) {
        User user = usersService.getCurrentUser();
        model.addAttribute("user", user);
        return "personalAccount";
    }

    @PostMapping("/personalAccount/{user-id}/update")
    public String update(@PathVariable("user-id") Integer userId, UsersForm usersForm) {
        usersService.update(userId, usersForm);
        return "redirect:/menu";
    }
}
