package urbanchik.attestation.pizzashop.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import urbanchik.attestation.pizzashop.forms.PizzaForm;
import urbanchik.attestation.pizzashop.services.PizzaService;

@Controller
@RequiredArgsConstructor
public class CreatePizzaController {

    private final PizzaService pizzaService;

    @GetMapping("/createPizza")
    public String getAddPizzaPage() {
        return "createPizza";
    }

    @PostMapping("/createPizza")
    public String addPizza(PizzaForm form) {
        pizzaService.addPizza(form);
        return "redirect:/adminPage";
    }
}
