package urbanchik.attestation.pizzashop.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import urbanchik.attestation.pizzashop.models.Cart;
import urbanchik.attestation.pizzashop.models.CartItem;
import urbanchik.attestation.pizzashop.services.CartService;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class CartController {

    private final CartService cartService;

    @GetMapping("/cart")
    public String geCartPage(Model model) {
        List<CartItem> cartItemList = cartService.getAllCartItem();
        model.addAttribute("cartItemList", cartItemList);
        Integer sumCost = cartService.getSumCost();
        model.addAttribute("sumCost", sumCost);
        Cart cart = cartService.getCurrentCart();
        model.addAttribute("cart", cart);
        cartService.updateSumCost();
        return "cart";
    }

    @PostMapping("/cart/{cartItem-id}/cartItem")
    public String deleteCartItem(@PathVariable("cartItem-id") Integer cartItemId) {
        cartService.deleteCartItem(cartItemId);
        cartService.updateSumCost();
        return "redirect:/cart";
    }

    @PostMapping("/cart/{cart-id}/clearCart")
    public String clearCart(@PathVariable("cart-id") Cart cart) {
        cartService.clearCart(cart);
        cartService.updateSumCost();
        return "redirect:/cart";
    }

}
