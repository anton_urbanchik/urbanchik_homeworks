package urbanchik.attestation.pizzashop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import urbanchik.attestation.pizzashop.models.Cart;
import urbanchik.attestation.pizzashop.models.CartItem;
import urbanchik.attestation.pizzashop.models.User;
import urbanchik.attestation.pizzashop.services.CartService;
import urbanchik.attestation.pizzashop.services.OrderService;
import urbanchik.attestation.pizzashop.services.UsersService;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class OrderController {

    private final CartService cartService;
    private final OrderService orderService;
    private final UsersService usersService;

    @GetMapping("/user_order")
    public String getOrderPage(Model model) {
        List<CartItem> cartItemList = cartService.getAllCartItem();
        model.addAttribute("cartItemList", cartItemList);
        orderService.setResultCost();
        Integer resultCost = orderService.getCurrentOrder().getResultCost();
        model.addAttribute("resultCost", resultCost);
        Cart cart = cartService.getCurrentCart();
        model.addAttribute("cart", cart);
        User user = usersService.getCurrentUser();
        model.addAttribute("user", user);
        if (cart.getSumCost() == 0) {
            return "redirect:/menu";
        } else {
            return "user_order";
        }
    }
}
