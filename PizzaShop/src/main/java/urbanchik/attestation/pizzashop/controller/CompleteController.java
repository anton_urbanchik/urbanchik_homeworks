package urbanchik.attestation.pizzashop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import urbanchik.attestation.pizzashop.models.Cart;
import urbanchik.attestation.pizzashop.models.User;
import urbanchik.attestation.pizzashop.services.CartService;
import urbanchik.attestation.pizzashop.services.OrderService;
import urbanchik.attestation.pizzashop.services.UsersService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

@Controller
@RequiredArgsConstructor
public class CompleteController {

    private final CartService cartService;
    private final OrderService orderService;
    private final UsersService usersService;

    /**
     * удаляет CartItem относящиеся к корзине из базы данных(очищает корзину)
     *
     * @return страницу окончания заказа
     */
    @GetMapping("/complete_order")
    public String getCompleteOrderPage(Model model) {
        Cart cart = cartService.getCurrentCart();
        cartService.clearCart(cart);
        cartService.updateSumCost();
        orderService.setDateTime();
        LocalDateTime localDateTime = orderService.getCurrentOrder().getDateTime();
        String formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).format(localDateTime);
        model.addAttribute("formatter", formatter);
        User user = usersService.getCurrentUser();
        model.addAttribute("user", user);
        return "complete_order";
    }
}
