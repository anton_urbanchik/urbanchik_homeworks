package urbanchik.attestation.pizzashop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import urbanchik.attestation.pizzashop.forms.CartForm;
import urbanchik.attestation.pizzashop.forms.OrderForm;
import urbanchik.attestation.pizzashop.forms.SignUpForm;
import urbanchik.attestation.pizzashop.models.User;
import urbanchik.attestation.pizzashop.services.CartService;
import urbanchik.attestation.pizzashop.services.OrderService;
import urbanchik.attestation.pizzashop.services.SignUpService;


@Controller
@RequiredArgsConstructor
@RequestMapping("/signUp")
public class SignUpController {

    private final SignUpService signUpService;
    private final CartService cartService;
    private final OrderService orderService;

    @GetMapping
    public String getSignUpPage() {
        return "signUp";
    }

    @PostMapping
    public String signUpUser(SignUpForm form, CartForm cartForm, OrderForm orderForm) {
        User user = signUpService.signUpUser(form);
        cartService.createUserCart(cartForm, user);
        orderService.createUserOrder(orderForm, user);
        return "redirect:/signIn";
    }
}
