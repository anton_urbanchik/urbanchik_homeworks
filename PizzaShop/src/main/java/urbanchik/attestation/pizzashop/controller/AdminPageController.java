package urbanchik.attestation.pizzashop.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import urbanchik.attestation.pizzashop.models.User;
import urbanchik.attestation.pizzashop.services.UsersService;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class AdminPageController {

    private final UsersService usersService;

    @GetMapping("/adminPage")
    public String getUsersPage(Model model) {
        List<User> users = usersService.getAllUsers();
        model.addAttribute("users", users);
        return "adminPage";
    }

}
