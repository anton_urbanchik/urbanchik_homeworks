package urbanchik.attestation.pizzashop.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "account")
@OnDelete(action = OnDeleteAction.CASCADE)
public class User {

    @Enumerated(value = EnumType.STRING)
    private Role role;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String firstName;
    private String city;
    private String address;
    @Column(unique = true)
    private String email;
    private String hashPassword;

    public enum Role {
        ADMIN, USER
    }
}
