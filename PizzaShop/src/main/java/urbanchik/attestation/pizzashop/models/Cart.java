package urbanchik.attestation.pizzashop.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "cart")
public class Cart {
    @OneToMany(mappedBy = "cart")
    public List<CartItem> cartItemList;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @OneToOne
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private User user;

    private Integer sumCost;

    public void addToCart(CartItem cartItem) {
        cartItemList.add(cartItem);
    }

}
