package urbanchik.attestation.pizzashop.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDetailsService userDetailsServiceImpl;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsServiceImpl).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();

        http.authorizeRequests()
                .antMatchers("/signUp").permitAll()
                .antMatchers("/index").permitAll()
                .antMatchers("/signIn").permitAll()
                .antMatchers("/cart").authenticated()
                .antMatchers("/complete_order").authenticated()
                .antMatchers("/createPizza").hasAuthority("ADMIN")
                .antMatchers("/adminPage").hasAuthority("ADMIN")
                .antMatchers("/menu").authenticated()
                .antMatchers("/personalAccount").authenticated()
                .antMatchers("/user_order").authenticated()
                .and()
                .formLogin(formLogin -> formLogin
                        .loginPage("/signIn")
                        .usernameParameter("email")
                        .passwordParameter("password")
                        .successHandler(new AuthenticationSuccessHandlerImpl()));
    }
}
