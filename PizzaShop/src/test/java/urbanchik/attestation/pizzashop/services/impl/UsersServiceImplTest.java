package urbanchik.attestation.pizzashop.services.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import urbanchik.attestation.pizzashop.models.User;
import urbanchik.attestation.pizzashop.repositories.UsersRepository;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UsersServiceImplTest {

    @InjectMocks
    UsersServiceImpl serviceUnderTest;

    @Mock
    private UsersRepository usersRepository;

    @Test
    void getAllUsers() {
        when(usersRepository.findAll()).thenReturn(List.of());
        List<User> userList = serviceUnderTest.getAllUsers();

        verify(usersRepository).findAll();
        assertEquals(List.of(), userList);
    }
}