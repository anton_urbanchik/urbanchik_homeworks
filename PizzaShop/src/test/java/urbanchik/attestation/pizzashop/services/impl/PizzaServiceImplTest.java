package urbanchik.attestation.pizzashop.services.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import urbanchik.attestation.pizzashop.forms.PizzaForm;
import urbanchik.attestation.pizzashop.models.Pizza;
import urbanchik.attestation.pizzashop.repositories.PizzaRepository;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PizzaServiceImplTest {

    @InjectMocks
    PizzaServiceImpl serviceUnderTest;
    @Mock
    private PizzaRepository pizzaRepository;
    @Mock
    private PizzaForm pizzaForm;

    @Test
    void testGetAllPizza() {
        when(pizzaRepository.findAll()).thenReturn(List.of());
        List<Pizza> pizzaList = serviceUnderTest.getAllPizza();

        verify(pizzaRepository).findAll();
        assertEquals(List.of(), pizzaList);
    }

    @Test
    void testAddPizza() {
        Pizza pizza = Pizza.builder()
                .cost(0)
                .build();
        serviceUnderTest.addPizza(pizzaForm);

        verify(pizzaRepository).save(pizza);
    }
}