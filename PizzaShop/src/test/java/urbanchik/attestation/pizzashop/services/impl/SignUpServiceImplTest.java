package urbanchik.attestation.pizzashop.services.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;
import urbanchik.attestation.pizzashop.forms.SignUpForm;
import urbanchik.attestation.pizzashop.models.User;
import urbanchik.attestation.pizzashop.repositories.UsersRepository;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class SignUpServiceImplTest {

    @InjectMocks
    SignUpServiceImpl serviceUnderTest;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private UsersRepository usersRepository;
    @Mock
    private SignUpForm signUpForm;

    @Test
    void signUpUser() {
        User user = User.builder()
                .role(User.Role.USER)
                .build();
        serviceUnderTest.signUpUser(signUpForm);

        verify(usersRepository).save(user);
        verify(passwordEncoder).encode(signUpForm.getPassword());
    }
}