package urbanchik.attestation.pizzashop.services.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import urbanchik.attestation.pizzashop.forms.OrderForm;
import urbanchik.attestation.pizzashop.models.Order;
import urbanchik.attestation.pizzashop.models.User;
import urbanchik.attestation.pizzashop.repositories.OrderRepository;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class OrderServiceImplTest {

    @InjectMocks
    OrderServiceImpl serviceUnderTest;
    @Mock
    private OrderRepository orderRepository;
    @Mock
    private User user;
    @Mock
    private OrderForm orderForm;

    @Test
    void createUserOrder() {
        Order order = Order.builder()
                .user(user)
                .build();

        serviceUnderTest.createUserOrder(orderForm, user);

        verify(orderRepository).save(order);
    }

}