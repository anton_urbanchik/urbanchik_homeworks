package urbanchik.attestation.pizzashop.services.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import urbanchik.attestation.pizzashop.forms.CartForm;
import urbanchik.attestation.pizzashop.models.Cart;
import urbanchik.attestation.pizzashop.models.User;
import urbanchik.attestation.pizzashop.repositories.CartItemRepository;
import urbanchik.attestation.pizzashop.repositories.CartRepository;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class CartServiceImplTest {

    @InjectMocks
    CartServiceImpl serviceUnderTest;
    @Mock
    private CartRepository cartRepository;
    @Mock
    private CartItemRepository cartItemRepository;
    @Mock
    private Cart cart;
    @Mock
    private User user;
    @Mock
    private CartForm cartForm;

    @Test
    void deleteCartItem() {
        serviceUnderTest.deleteCartItem(1);
        verify(cartItemRepository).deleteById(1);
    }

    @Test
    void clearCart() {
        serviceUnderTest.clearCart(cart);
        verify(cartItemRepository).deleteByCart_Id(0);
    }

    @Test
    void createUserCart() {
        Cart cart = Cart.builder()
                .user(user)
                .build();
        serviceUnderTest.createUserCart(cartForm, user);
        verify(cartRepository).save(cart);
    }

}